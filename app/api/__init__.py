from fastapi import APIRouter, Depends

from app.lib.oauth2 import get_current_active_user

from .home import r as public_router
from .auth import r as auth_router

from .user import r as user_router
from .student import r as student_router
from .cooker import r as cook_router
from .incumbent import r as incumbent_router
from .rollcall import r as rollcall_router
from .rollcallitem import r as rollcallitem_router

api_routes = APIRouter(prefix="/api", dependencies=[Depends(get_current_active_user)])

api_routes.include_router(user_router)
api_routes.include_router(student_router)
api_routes.include_router(cook_router)
api_routes.include_router(incumbent_router)
api_routes.include_router(rollcall_router)
api_routes.include_router(rollcallitem_router)


@public_router.get("/api/ping")
async def ping_pong():
    return {"ping": "pong!"}
