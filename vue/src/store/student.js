import axios from "@/plugins/axios";

const SET_STUDENTS = "SET_STUDENTS"

const state = {
  students: [],
};

const getters = {
  students: state => state.students,
  student_by_id: (state) => (id) => state.students.find(s => s.id == id),
  student_by_no: (state) => (no) => state.students.find(s => s.no == no),

  get_students: async function (state) {
    if (state.students.length != 0 && state.students != null) {
      return state.students;
    }

    // return axios.get(`/api/students`).then( res => {state.students = res.data; return res.data});

    let res = await axios.get(`/api/students`)
    state.students = res.data;
    return state.students;
  }
};

const mutations = {
  [SET_STUDENTS] (state, students) {
    state.students = students;
  },
};

const actions = {
  [SET_STUDENTS] ({ commit }, students) {
    commit(SET_STUDENTS, students);
  },
};

const  student = { state, getters, actions, mutations };

export default student
