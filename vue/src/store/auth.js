// import axios from 'axios';

const USER = "USER";
const ACCESS_TOKEN = "ACCESS_TOKEN";
const SET_USER = "SET_USER";
const LOG_OUT = "LOG_OUT";
const LOG_IN = "LOG_IN";
const SET_ACCESS_TOKEN = "SET_ACCESS_TOKEN"

const state = {
  user: JSON.parse(localStorage.getItem("USER")) || null,
  ACCESS_TOKEN: localStorage.getItem("ACCESS_TOKEN") || null,
};

const mutations = {
  [SET_USER] (state, user) {
    state.user = user;
  },

  [ACCESS_TOKEN] (state, access_token) {
    state.ACCESS_TOKEN = access_token;
  },
};

const getters = {
  isAuthenticated: state => !!state.user,
  StateUser: state => state.user,
};

const actions = {
  [SET_USER] ({ commit }, user) {
    commit(SET_USER, user);
    localStorage.setItem('USER', JSON.stringify(user))
  },

  [SET_ACCESS_TOKEN] ({ commit }, access_token) {
    commit(ACCESS_TOKEN, access_token);
    localStorage.setItem(ACCESS_TOKEN, access_token)
  },

  [LOG_OUT] ({ dispatch }){
    dispatch(SET_USER, null)
    dispatch(SET_ACCESS_TOKEN, null)
  },

  [LOG_IN] ({ dispatch }, user, access_token){
    dispatch(SET_USER, user)
    dispatch(SET_ACCESS_TOKEN, access_token)
  },

  [LOG_OUT] ({ dispatch }){
    dispatch(SET_USER, null)
    dispatch(SET_ACCESS_TOKEN, null)
    localStorage.removeItem(USER);
    localStorage.removeItem(ACCESS_TOKEN);
  },
};

const  auth = { state, getters, actions, mutations };
export default auth
