from functools import lru_cache
import sqlalchemy.orm.session

import app.core.config
import app.core.db


@lru_cache()
def get_settings() -> app.core.config.Settings:
    return app.core.config.Settings()


def get_db() -> sqlalchemy.orm.session.Session:
    db = app.core.db.SessionLocal()
    try:
        yield db
    finally:
        db.close()
