import datetime
from sqlalchemy import Column, Text, ForeignKey, Date, Integer
from sqlalchemy.orm import relationship

from app.core.db import Base


class Incumbent(Base):
    __tablename__ = "incumbents"

    id = Column(Integer, primary_key=True, index=True)
    imam_id = Column(Integer, ForeignKey("students.id"))
    muezzin_id = Column(Integer, ForeignKey("students.id"))
    hatib_id = Column(Integer, ForeignKey("students.id"))
    nobetci1_id = Column(Integer, ForeignKey("students.id"))
    nobetci2_id = Column(Integer, ForeignKey("students.id"))
    note = Column(Text, nullable=True)
    date = Column(Date, default=datetime.date.today, unique=True)

    imam = relationship("Student", foreign_keys=[imam_id])
    muezzin = relationship("Student", foreign_keys=[muezzin_id])
    hatib = relationship("Student", foreign_keys=[hatib_id])
    nobetci1 = relationship("Student", foreign_keys=[nobetci1_id])
    nobetci2 = relationship("Student", foreign_keys=[nobetci2_id])
