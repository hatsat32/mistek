from fastapi import FastAPI

from app.initializers import init
from app.core.deps import get_settings


def create_app(version: str) -> FastAPI:
    s = get_settings()

    application = FastAPI(
        title=s.APP_NAME,
        description=s.DESCRIPTION,
        debug=s.DEBUG,
        version=version,
        openapi_url=s.OPENAPI_URL
    )

    init(application)

    return application
