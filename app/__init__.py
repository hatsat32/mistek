from .main import create_app

__VERSION__: str = "0.1.1"

app = create_app(__VERSION__)
