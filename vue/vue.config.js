module.exports = {
  // publicPath: process.env.NODE_ENV === 'production' ? '/static/' : '/',
  outputDir: process.env.NODE_ENV === 'production' ? '../public/' : 'dist',
  devServer: {
    // proxy: 'http://localhost:8000'
    proxy: {
      "^/api": {
        target: "http://localhost:8000",
        ws: true,
        changeOrigin: true
      },
    }
  }
}
