import datetime
from sqlalchemy import Column, ForeignKey, Integer, Date, UniqueConstraint
from sqlalchemy.orm import relationship

from app.core.db import Base


class Cooker(Base):
    __tablename__ = "cookers"

    id = Column(Integer, primary_key=True, index=True)
    student_id = Column(Integer, ForeignKey("students.id"))
    date = Column(Date, default=datetime.date.today)

    student = relationship("Student", foreign_keys=student_id)

    __table_args__ = (
        UniqueConstraint("student_id", "date"),
    )
