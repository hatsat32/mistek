from sqlalchemy import Column, Integer

from core.db import Base


class Guard(Base):
    __tablename__ = "guards"

    id = Column(Integer, primary_key=True)
