from sqlalchemy import Column, Integer, String

from app.core.db import Base


class Student(Base):
    __tablename__ = "students"

    id = Column(Integer, primary_key=True)
    no = Column(Integer, unique=True)
    fullname = Column(String)
