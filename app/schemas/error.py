from pydantic import BaseModel
from typing import List, Optional


class ErrorResponse(BaseModel):
    status: Optional[int]
    type: str
    message: int
    detail: str


class ErrorResponseMultiple(BaseModel):
    errors: List[ErrorResponse]
