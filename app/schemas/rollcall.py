import datetime
from pydantic import BaseModel
from typing import Optional, List

from .student import StudentOut


class RollCallBase(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None
    date: datetime.date


class RollCallUpdate(RollCallBase):
    pass


class RollCallOut(RollCallBase):
    id: Optional[int]

    class Config:
        orm_mode = True


class RollCallJoinOut(RollCallOut):
    items: Optional[List[StudentOut]] = None


class RollCallInDB(RollCallBase):
    id: int

    class Config:
        orm_mode = True
