from pydantic import BaseModel
from typing import Optional


class StudentBase(BaseModel):
    fullname: str
    no: Optional[int]


class StudentUpdate(StudentBase):
    pass


class StudentOut(StudentBase):
    id: Optional[int]

    class Config:
        orm_mode = True


class StudentInDB(StudentBase):
    id: int

    class Config:
        orm_mode = True
