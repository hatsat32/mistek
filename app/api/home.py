from fastapi import APIRouter
from fastapi.responses import HTMLResponse


r = APIRouter()


@r.get("/", response_class=HTMLResponse)
async def root():
    try:
        f = open("../public/index.html")
        return f.read()
    except FileNotFoundError:
        return HTMLResponse("<h1>Not Found<h1>", status_code=200)
