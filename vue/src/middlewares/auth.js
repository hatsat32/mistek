export default function(next, store) {
  if (! store.state.auth.ACCESS_TOKEN) {
    next("/login");
  } else {
    next();
  }
}
