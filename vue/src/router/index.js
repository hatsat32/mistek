import { createRouter, createWebHistory } from 'vue-router'
import store from "../store/index";

import Dashboard from "../views/Dashboard";
import DashboardLayout from "@/layouts/DashboardLayout";
import PublicLayout from "@/layouts/PublicLayout";

const routes = [
  {
    path: '',
    name: 'DashboardLayout',
    component: DashboardLayout,
    meta: { middleware: "auth" },
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('../views/user/Users.vue')
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('../views/user/Users.vue')
      },
      {
        path: '/users/new',
        name: 'UsersNew',
        component: () => import('../views/user/New.vue')
      },
      {
        path: '/users/:id',
        name: 'UsersDetail',
        component: () => import('../views/user/Detail.vue')
      },
      {
        path: '/students',
        name: 'Students',
        component: () => import('../views/student/Index.vue')
      },
      {
        path: '/students/:id',
        name: 'StudentDetail',
        component: () => import('../views/student/Detail.vue')
      },
      {
        path: '/cooker',
        name: 'Cooker',
        component: () => import('../views/cooker/Cooker.vue')
      },
      {
        path: '/cookers/:date',
        name: 'CookerDetail',
        component: () => import('../views/cooker/Detail.vue')
      },
      {
        path: '/incumbent',
        name: 'Incumbent',
        component: () => import('../views/incumbent/Incumbent.vue')
      },
      {
        path: '/incumbent/new',
        name: 'IncumbentNew',
        component: () => import('../views/incumbent/New.vue')
      },
      {
        path: '/incumbent/:id',
        name: 'IncumbentDetail',
        component: () => import('../views/incumbent/Detail.vue')
      },
      {
        path: '/about',
        name: 'About',
        component: () => import('../views/About.vue')
      },
      {
        path: '/rollcall',
        name: 'RollCall',
        component: () => import('../views/rollcall/RollCall.vue'),
      },
      {
        path: "/rollcall/:id",
        name: 'RollCallDetail',
        component: () => import('../views/rollcall/RollCallDetail.vue')
      }
    ]
  },
  {
    path: '',
    name: 'PublicLayout',
    component: PublicLayout,
    children: [
      {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/auth/Login.vue')
      },
    ]
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, _, next) => {
  if (to.meta.middleware) {
    const middleware = require(`@/middlewares/${to.meta.middleware}`);
    if (middleware) {
      middleware.default(next, store);
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router
