from fastapi import APIRouter, Depends, status, HTTPException
import datetime
from typing import List
from sqlalchemy.orm import Session, joinedload

from app.core.deps import get_db
from app import models
from app.schemas import IncumbentOut, IncumbentCreate, IncumbentBase, IncumbentToday


r = APIRouter(prefix="/incumbents", tags=["Incumbent"])


@r.get("", response_model=List[IncumbentOut])
async def incumbents(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return db.query(models.Incumbent).offset(skip).limit(limit).all()


@r.post("", response_model=IncumbentOut, status_code=status.HTTP_201_CREATED, summary="Save a cooker")
async def incumbent_save(incumbent: IncumbentCreate, db: Session = Depends(get_db)):
    inc = models.Incumbent(**incumbent.dict())

    db.add(inc)
    db.commit()
    db.refresh(inc)

    return inc


@r.get("/{incumbent_id:int}", response_model=IncumbentOut)
async def incumbent_detail(incumbent_id: int, db: Session = Depends(get_db)):
    incumbent = db.query(models.Incumbent).filter(models.Incumbent.id == incumbent_id).first()
    if not incumbent:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    return incumbent


@r.put("/{incumbent_id:int}", response_model=IncumbentOut)
async def incumbent_update(incumbent_id: int, incumbent: IncumbentBase, db: Session = Depends(get_db)):
    data = incumbent.dict(exclude_unset=True)
    inc = db.query(models.Incumbent).filter(models.Incumbent.id == incumbent_id).first()
    if not inc:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    db.query(models.Incumbent).filter(models.Incumbent.id == incumbent_id).update(data)
    db.commit()

    inc.__dict__.update(data)
    return inc


@r.get("/today", response_model=IncumbentToday)
async def incumbents_today(db: Session = Depends(get_db)):
    incumbent = db.query(models.Incumbent).options(joinedload("*")).filter(
        models.Incumbent.date == datetime.date.today()
    ).first()

    return {"incumbent": incumbent, "today": datetime.date.today()}
