from fastapi import APIRouter, Depends, status, HTTPException
import datetime
from typing import List
from sqlalchemy.orm import Session, joinedload

from app.schemas import RollCallBase, RollCallOut
from app.core.deps import get_db
from app import models


r = APIRouter(prefix="/rollcalls", tags=["Roll Call"])


@r.get("", response_model=List[RollCallOut], summary="Get roll call list")
async def roll_call_list(db: Session = Depends(get_db)):
    return db.query(models.RollCall).order_by(models.RollCall.date.desc()).all()


@r.post("", response_model=RollCallOut, status_code=status.HTTP_201_CREATED, summary="Save a roll call")
async def create(rc: RollCallBase, db: Session = Depends(get_db)):
    rollcall = models.RollCall(title=rc.title, date=rc.date)

    db.add(rollcall)
    db.commit()
    db.refresh(rollcall)

    return rollcall


@r.get("/{rollcall_id}", response_model=RollCallOut, summary="Get roll call details")
async def rollcall_detail(rollcall_id: int, db: Session = Depends(get_db)):
    """
    Get roll call by id.
    """
    rollcall = db.query(models.RollCall).filter(models.RollCall.id == rollcall_id).first()
    return rollcall


@r.put("/{rollcall_id}", response_model=RollCallOut, summary="Update a roll call.")
async def update_rollcall(rollcall_id: int, rollcall: RollCallBase, db: Session = Depends(get_db)):
    rc = db.query(models.RollCall).filter(models.RollCall.id == rollcall_id).first()
    if not rc:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    db.query(models.RollCall).filter(models.RollCall.id == rollcall_id).update(rollcall.dict(exclude_unset=True))
    db.commit()

    rc.__dict__.update(rollcall.dict(exclude_unset=True))
    return rc


@r.delete("/{rollcall_id}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a roll")
async def delete_rollcall(rollcall_id: int, db: Session = Depends(get_db)):
    """
    Delete a roll coll

    - **rollcall_id** The if of rollcall to delete.
    """
    db.query(models.RollCall).filter(models.RollCall.id == rollcall_id).delete()
    db.commit()


@r.get("/{rollcall_id}/items", response_model=List)
async def get_items(rollcall_id: int, join: bool = False, db: Session = Depends(get_db)):
    query = db.query(models.RollCallItem)
    if join:
        query = query.options(joinedload("*"))
    return query.filter(models.RollCallItem.roll_call_id == rollcall_id).all()


@r.get("/date/{date}", response_model=List[RollCallOut], summary="Get roll calls in a date")
async def cooker_date(date: datetime.date, db: Session = Depends(get_db)):
    return db.query(models.RollCall).filter(models.RollCall.date == date).all()
