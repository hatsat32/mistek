from fastapi import APIRouter, Depends, Body, status, HTTPException
import datetime
from typing import List
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

from app import models
from app.schemas import cooker_schema, student_schema
from app.core.deps import get_db


r = APIRouter(prefix="/cookers", tags=["Cooker"])


@r.get("", response_model=List[cooker_schema.CookerOut], summary="Get cooker list")
async def get(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return db.query(models.Cooker).offset(skip).limit(limit).all()


@r.post("", response_model=cooker_schema.CookerOut, status_code=status.HTTP_201_CREATED, summary="Save a cooker")
async def set_cookers(co: cooker_schema.CookerBase, db: Session = Depends(get_db)):
    cooker = models.Cooker(student_id=co.student_id, date=co.date)

    try:
        db.add(cooker)
        db.commit()
        db.refresh(cooker)
    except IntegrityError:
        raise HTTPException(status.HTTP_409_CONFLICT)

    return cooker


@r.get("/today", response_model=cooker_schema.CookerToday, summary="Bu güne ait yemekçileri al")
async def cooker_today(db: Session = Depends(get_db)):
    today = datetime.date.today()
    cookers = db.query(models.Student).join(models.Cooker).filter(
        models.Cooker.date == today
    ).all()
    return {"cookers": cookers, "today": today}


@r.get("/{cooker_id}", response_model=cooker_schema.CookerOut, summary="Get cooker by id")
async def cooker_detail(cooker_id: int, db: Session = Depends(get_db)):
    """
    Get cooker by id.
    """
    cooker = db.query(models.Cooker).filter(models.Cooker.id == cooker_id).first()
    if not cooker:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    return cooker


@r.get("/date/{date}", response_model=List[cooker_schema.CookerOut], summary="Get cookers in a date")
async def cooker_date(date: datetime.date, db: Session = Depends(get_db)):
    return db.query(models.Cooker).filter(models.Cooker.date == date).all()


@r.get("/date/{date}/students", response_model=List[student_schema.StudentOut], summary="Get cookers in a date")
async def cooker_date_students(date: datetime.date, db: Session = Depends(get_db)):
    return db.query(models.Student).join(models.Cooker).filter(
        models.Cooker.date == date
    ).all()


@r.post("/date/{date}", status_code=status.HTTP_201_CREATED, summary="Set cookers in a date")
async def cooker_date_save(date: datetime.date, student_ids: List[int] = Body(None), db: Session = Depends(get_db)):
    cookers = [models.Cooker(student_id=s, date=date) for s in student_ids]
    db.bulk_save_objects(cookers)
    db.commit()


@r.delete("/date/{date}/{student_id}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete cooker in the day")
async def cooker_date_delete(date: datetime.date, student_id: int, db: Session = Depends(get_db)):
    db.query(models.Cooker).filter(
        models.Cooker.student_id == student_id, models.Cooker.date == date
    ).delete()
    db.commit()
