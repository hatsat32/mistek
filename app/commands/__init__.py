import typer
import uvicorn
from app.main import create_app
from app import __VERSION__
from . import user


app = typer.Typer()

app.add_typer(user.app, name="users", help="User management")


@app.command("run")
def run_app():
    """
    Run MiSTek App
    """
    uvicorn.run(create_app(__VERSION__))
