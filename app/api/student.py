from fastapi import APIRouter, Depends, status, HTTPException
from typing import List
from sqlalchemy.orm import Session

from app.core.deps import get_db
from app.models import Student
from app.schemas import StudentBase, StudentUpdate, StudentOut


r = APIRouter(tags=["Student"])


@r.get("/students", response_model=List[StudentOut])
async def students(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    all_students = db.query(Student).offset(skip).limit(limit).all()
    return all_students


@r.post("/students", response_model=StudentOut, status_code=status.HTTP_201_CREATED)
async def student_save(st: StudentBase, db: Session = Depends(get_db)):
    s = Student(no=st.no, fullname=st.fullname)
    db.add(s)
    db.commit()
    db.refresh(s)
    return s


@r.get("/students/{student_id}", response_model=StudentOut)
async def student_detail(student_id: int, db: Session = Depends(get_db)):
    student = db.query(Student).filter(Student.id == student_id).first()
    if not student:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    return student


@r.put("/students/{student_id}", response_model=StudentOut)
async def student_update(student_id: int, student: StudentUpdate, db: Session = Depends(get_db)):
    s = db.query(Student).filter(Student.id == student_id).first()
    if not s:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    db.query(Student).filter(Student.id == student_id).update(student.dict(exclude_unset=True))
    db.commit()

    s.__dict__.update(student.dict(exclude_unset=True))
    return s


@r.delete("/students/{student_id}", status_code=status.HTTP_204_NO_CONTENT)
async def student_delete(student_id: int, db: Session = Depends(get_db)):
    db.query(Student).filter(Student.id == student_id).delete()
    db.commit()
