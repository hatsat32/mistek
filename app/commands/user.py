import typer
from sqlalchemy.orm import Session

from app.core.db import SessionLocal
from app.models import User


app = typer.Typer()


@app.command("create")
def create_user(username: str, email: str, password: str):
    """
    Create a new user and save to database
    """

    u = User(username=username, email=email, is_active=True)
    u.set_password(password)

    db: Session = SessionLocal()
    db.add(u)
    db.commit()
    db.refresh(u)

    typer.echo(u)
    typer.echo(f"User '{username}' Created!")


@app.command("list")
def list_users():
    """
    List users in db.
    """
    db: Session = SessionLocal()
    users = db.query(User).all()
    for u in users:
        typer.echo(f"ID[{u.id}] USERNAME[{u.username}] EMAIL[{u.email}] ACTIVE[{u.is_active}]")
