import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toast, { PluginOptions } from "./plugins/toastification"

createApp(App).use(store).use(router).use(Toast, PluginOptions).mount('#app')
