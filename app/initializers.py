from fastapi import FastAPI
from pathlib import Path
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from starlette.responses import JSONResponse

from app.api import api_routes, public_router, auth_router
from app.core.exceptions import HTTPException


def init_routes(app: FastAPI):
    app.include_router(auth_router)
    app.include_router(api_routes)
    app.include_router(public_router)


def init_cors(app: FastAPI):
    origins = [
        "http://localhost",
        "http://localhost:8080",
    ]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def init_exceptions(app: FastAPI):
    @app.exception_handler(HTTPException)
    def app_exception_handler(exc: HTTPException) -> JSONResponse:
        return JSONResponse(
            {"detail": exc.detail, "message": exc.message},
            status_code=exc.status_code,
            headers=exc.headers
        )


def init_frontend(app: FastAPI):
    if Path.cwd().parent.joinpath("public").is_dir():
        app.mount("/", StaticFiles(directory="../public"), name="public")


def init(app: FastAPI):
    init_routes(app)
    init_cors(app)
    init_exceptions(app)
    init_frontend(app)
