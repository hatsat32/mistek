import Toast from "vue-toastification";

// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

export const PluginOptions = {
	// You can set your default options here
	hideProgressBar: true,
	transition: "Vue-Toastification__fade",
	timeout: 5000,
};

export default Toast
