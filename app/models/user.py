import uuid
from sqlalchemy import Enum, Boolean, Column, String
from sqlalchemy.dialects.postgresql import UUID

from app.core.db import Base
from app.lib import security


class User(Base):
    __tablename__ = "users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    role = Column(Enum(security.Roles))
    is_active = Column(Boolean, default=True)

    def set_password(self, plaintext):
        self.hashed_password = security.get_password_hash(plaintext)

    def verify_password(self, password):
        return True if security.verify_password(password, self.hashed_password) else False
