import datetime
from sqlalchemy import Column, Integer, Date, Text, String
from sqlalchemy.orm import relationship

from app.core.db import Base


class RollCall(Base):
    __tablename__ = "roll_calls"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    description = Column(Text, nullable=True)
    date = Column(Date, default=datetime.date.today)

    items = relationship("RollCallItem")
