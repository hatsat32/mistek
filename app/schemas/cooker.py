import datetime
from pydantic import BaseModel
from typing import Optional, List

from .student import StudentOut


class CookerBase(BaseModel):
    student_id: int
    date: Optional[datetime.date]


class CookerUpdate(CookerBase):
    pass


class CookerOut(CookerBase):
    id: Optional[int]

    class Config:
        orm_mode = True


class CookerToday(BaseModel):
    cookers: List[StudentOut]
    today: datetime.date


class CookerInDB(CookerBase):
    id: int

    class Config:
        orm_mode = True
