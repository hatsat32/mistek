from pydantic import BaseModel
from typing import Optional

from .student import StudentOut


class RollCallItemBase(BaseModel):
    roll_call_id: int
    student_id: int


class RollCallItemUpdate(RollCallItemBase):
    pass


class RollCallItemOut(RollCallItemBase):
    id: Optional[int]

    class Config:
        orm_mode = True


class RollCallItemJoinOut(RollCallItemOut):
    student: Optional[StudentOut] = None


class RollCallItemInDB(RollCallItemBase):
    id: int

    class Config:
        orm_mode = True
