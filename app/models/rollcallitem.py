from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship

from app.core.db import Base


class RollCallItem(Base):
    __tablename__ = "roll_call_items"

    id = Column(Integer, primary_key=True, index=True)
    roll_call_id = Column(Integer, ForeignKey("roll_calls.id"))
    student_id = Column(Integer, ForeignKey("students.id"))

    student = relationship("Student", foreign_keys=student_id)
