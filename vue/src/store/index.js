import { createStore } from 'vuex'
// import * as types from './types';
import auth from './auth';
import student from "./student";

const store = createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth, student
  }
})

export default store
