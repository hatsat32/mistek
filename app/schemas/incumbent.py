import datetime
from typing import Optional
from pydantic import BaseModel


class IncumbentBase(BaseModel):
    imam_id: int
    muezzin_id: int
    hatib_id: int
    nobetci1_id: int
    nobetci2_id: int
    note: Optional[str]
    date: Optional[datetime.date]


class IncumbentCreate(IncumbentBase):
    pass


class IncumbentUpdate(IncumbentBase):
    pass


class IncumbentOut(IncumbentBase):
    id: int

    class Config:
        orm_mode = True


class IncumbentToday(BaseModel):
    incumbent: IncumbentOut
    today: datetime.date


class IncumbentInDB(IncumbentBase):
    id: int
