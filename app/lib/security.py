import enum
from passlib.pwd import genword
from passlib.hash import argon2


class Roles(enum.Enum):
    """
    Roles used in mistek.
    """
    admin = "admin"
    manager = "manager"
    president = "president"


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Return True if password is true. False otherwise."""
    return argon2.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    """Get argon2id password hash from password."""
    return argon2.hash(password)


def generate_password():
    """Generate random password."""
    return genword()
