from pydantic import BaseModel, EmailStr, SecretStr, validator, UUID4
from typing import Optional

from app.lib.security import Roles


class UserBase(BaseModel):
    username: str
    email: EmailStr
    role: Optional[Roles]
    is_active: Optional[bool] = True


class UserCreate(BaseModel):
    username: str
    email: EmailStr
    password: str
    password_confirm: str

    @validator('password_confirm')
    def passwords_match(cls, v, values):
        if 'password' in values and v != values['password']:
            raise ValueError('Passwords do not match!')
        return v


class UserUpdate(BaseModel):
    username: str
    email: EmailStr


class UserOut(UserBase):
    id: UUID4

    class Config:
        orm_mode = True


class UserInDB(UserOut):
    id: UUID4
    hashed_password: SecretStr
