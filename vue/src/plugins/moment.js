import moment from "moment"

moment.locale("tr")

export const human_date = date => moment(date).format("LL")

export default moment;
