from fastapi import APIRouter, Depends, status, HTTPException
from typing import List
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

from app.core.deps import get_db
from app.schemas import RollCallItemBase, RollCallItemOut
from app import models


r = APIRouter(prefix="/rollcallitems", tags=["Roll Call Item"])


@r.get("", response_model=List[RollCallItemOut], summary="Get roll call item list")
async def rollcallitem_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return db.query(models.RollCallItem).offset(skip).limit(limit).all()


@r.post("", response_model=RollCallItemOut, status_code=status.HTTP_201_CREATED, summary="Save a roll call item")
async def create(rci: RollCallItemBase, db: Session = Depends(get_db)):
    rollcallitem = models.RollCallItem(**rci.dict())

    try:
        db.add(rollcallitem)
        db.commit()
        db.refresh(rollcallitem)
    except IntegrityError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST)

    return rollcallitem


@r.get("/{rollcallitem_id}", response_model=RollCallItemOut, summary="Get roll call item details")
async def rollcall_detail(rollcallitem_id: int, db: Session = Depends(get_db)):
    """
    Get roll call by id.
    """
    rollcallitem = db.query(models.RollCallItem).filter(models.RollCallItem.id == rollcallitem_id).first()
    if not rollcallitem:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    return rollcallitem


@r.put("/{rollcallitem_id}", response_model=RollCallItemOut, summary="Update a roll call item.")
async def update_rollcall(rollcallitem_id: int, rollcallitem: RollCallItemBase, db: Session = Depends(get_db)):
    rci = db.query(models.RollCallItem).filter(models.RollCallItem.id == rollcallitem_id).first()
    if not rci:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    db.query(models.RollCallItem).filter(
        models.RollCallItem.id == rollcallitem_id
    ).update(rollcallitem.dict(exclude_unset=True))
    db.commit()

    rci.__dict__.update(rollcallitem.dict(exclude_unset=True))
    return rci


@r.delete("/{rollcallitem_id}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a roll call item")
async def delete_rollcall(rollcallitem_id: int, db: Session = Depends(get_db)):
    """
    Delete a roll coll

    - **rollcallitem_id** The if of rollcall to delete.
    """
    db.query(models.RollCallItem).filter(models.RollCallItem.id == rollcallitem_id).delete()
    db.commit()
